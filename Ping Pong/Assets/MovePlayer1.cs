﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer1 : MonoBehaviour
{

    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public float limitLeft = -4.85f;
    public float limitRight = -7.7f;
    public bool playerOne = true;
    float translation;
    public KeyCode playerOneMoveUp = KeyCode.W;
    public KeyCode playerOneMoveDown = KeyCode.S;
    public KeyCode playerTwoMoveUp = KeyCode.UpArrow;
    public KeyCode playerTwoMoveDown = KeyCode.DownArrow;

    public GameObject playerPrefab;
    List<GameObject> players = new List<GameObject>();



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        foreach (GameObject player in GameObject.FindObjectsOfType(typeof(GameObject)))
        {
            if (player.tag == "Player" && !players.Contains(player))
                players.Add(player);
        }


        if (Input.GetKey(playerOneMoveDown))
        {
            if (players[0].transform.position.z > limitLeft)
            {
                players[0].transform.Translate(0, 0, 0);
            }
            else
            {
                players[0].transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }

        }
            if (Input.GetKey(playerOneMoveUp))
            {
            if (players[0].transform.position.z < limitRight)
            {
                players[0].transform.Translate(0, 0, 0);
            }
            else
            {
                players[0].transform.Translate(Vector3.back * speed * Time.deltaTime);
            }

 
        }
        if (Input.GetKey(playerTwoMoveUp))
        {

            if (players[1].transform.position.z > limitLeft)
            {
                players[1].transform.Translate(0, 0, 0);
            }
            else
            {
                players[1].transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }
        }
        if (Input.GetKey(playerTwoMoveDown)) {
            if (players[1].transform.position.z < limitRight)
            {
                players[1].transform.Translate(0, 0, 0);
            }
            else
            {
                players[1].transform.Translate(Vector3.back * speed * Time.deltaTime);
            }
        }
    }
}
