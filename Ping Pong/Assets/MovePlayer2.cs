﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer2 : MonoBehaviour
{
    public float speedP2  = 10.0f;
    public float rotationSpeed2 = 100.0f;
    public float limitLeftP2 = -4.85f;
    public float limitRightP2 = -7.7f;
    public KeyCode moveUp = KeyCode.UpArrow;
    public KeyCode moveDown = KeyCode.DownArrow;
    public float translationP2;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(moveUp))
        {
            translationP2 = Input.GetAxis("Vertical") * speedP2;

        }
        if (Input.GetKey(moveDown))
        {
            
            translationP2 = Input.GetAxis("Vertical")*speedP2;

        }



        // Make it move 10 meters per second instead of 10 meters per frame...
        translationP2 *= Time.deltaTime;

        // Move translation along the object's z-axis
        //transform.Translate(0, 0, translation);
        if (translationP2 > 0 && transform.position.z < limitLeftP2)
        {
            transform.Translate(0, 0, translationP2);
        }

        if (translationP2 < 0 && transform.position.z > limitRightP2)
        {
            transform.Translate(0, 0, translationP2);

        }
    }
}
