﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    AudioSource audioSource;

    public float thrust = 50.0f;
    public Rigidbody rb;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        rb = GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(3*thrust,0, thrust));
    }

    void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            Debug.DrawRay(contact.point, contact.normal, Color.white);
        }
        if (collision.relativeVelocity.magnitude > 2)
        {

            Vector3 direction = rb.transform.position - transform.position;

            if (Vector3.Dot(transform.forward, direction) > 0)
            {
              //  rb.AddForce(new Vector3(15 * thrust, 0,- 15 * thrust));
            }
            if (Vector3.Dot(transform.forward * -1, direction) < 0)
            {
                 //rb.AddForce(new Vector3(15 * thrust, 0,- 15 * thrust));
            }
            if (Vector3.Dot(transform.forward, direction) == 0)
            {
              rb.AddForce(new Vector3(-15* thrust, 0, 15 * thrust));
            }
            audioSource.Play();
        }

 
  
    }
    void OnTriggerEnter3D(Collider3D other)
    {
        if (other.Tag == "Player1")
        { 
        
        }
        if (other.Tag == "Player2")
        { 
        
        }
    }

}
